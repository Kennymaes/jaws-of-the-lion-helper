package models

import kotlinx.datetime.Clock.System.now
import kotlinx.datetime.LocalDate
import kotlin.random.Random.Default.nextLong

data class Game(val id: Long = nextLong(), val slot: Number, val name: String, val creationDate: String? = now().toString(), val players: List<Player> = emptyList()) {

    fun playerSize(): Number {
        return players.size;
    }

}