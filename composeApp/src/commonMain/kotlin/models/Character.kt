package models

data class Character(val name: String, val imageName: String, val health: Number, val maxCards: Number)