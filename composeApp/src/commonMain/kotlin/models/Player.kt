package models

import kotlin.random.Random.Default.nextLong

data class Player(val playerId: Long = nextLong(), val name: String, val stats: Stats)