package models

import kotlin.random.Random.Default.nextLong

data class Stats(val statsId: Long? = nextLong(), val health: Number, val mana: Number, val coins: Number) {
    fun reduceHealth(): Stats {
        return this.copy(health = health.toInt() - 1)
    }

    fun reduceMana(): Stats {
        return this.copy(mana = mana.toInt() - 1)
    }

    fun reduceCoins(): Stats {
        return this.copy(coins = coins.toInt() - 1)
    }

    fun addHealth(): Stats {
        return this.copy(health = health.toInt() + 1)
    }

    fun addMana(): Stats {
        return this.copy(mana = mana.toInt() + 1)
    }

    fun addCoins(): Stats {
        return this.copy(coins = coins.toInt() + 1)
    }
}