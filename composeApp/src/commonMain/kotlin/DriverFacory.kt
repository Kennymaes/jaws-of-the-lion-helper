import app.cash.sqldelight.db.SqlDriver
import be.kennymaes.database.AppDatabase

expect class DriverFactory {
    fun createDriver(): SqlDriver
}

fun createDatabase(driver: SqlDriver): AppDatabase {
    return AppDatabase(driver)
}