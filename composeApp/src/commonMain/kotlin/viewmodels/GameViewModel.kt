package viewmodels

import dev.icerock.moko.mvvm.viewmodel.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import models.Game
import models.Stats
import repository.GameRepository
import repository.PlayerRepository

class GameViewModel(
    private val id: Long,
    private val gameRepository: GameRepository,
    private val playerRepository: PlayerRepository
) : ViewModel() {
    private val _game = MutableStateFlow<Game?>(null)
    val game = _game.asStateFlow()

    init {
        viewModelScope.launch {
            loadData()
            println(_game)
        }
    }

    fun saveStats(stats: Stats, playerId: Long) {
        playerRepository.saveStats(stats, playerId)
        loadData()
    }

    fun loadData() {
        val game = gameRepository.getById(id)
        _game.update { game }
    }

    fun delete(id: Long) {
        gameRepository.delete(id)
    }
}