package viewmodels

import dev.icerock.moko.mvvm.viewmodel.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import models.Game
import models.Player
import repository.GamePlayerRepository
import repository.GameRepository
import repository.PlayerRepository

class GamesViewModel(private val gameRepository: GameRepository, private val playerRepository: PlayerRepository, val gamePlayerRepository: GamePlayerRepository) : ViewModel() {
    private val _games = MutableStateFlow<List<Game>>(emptyList())
    val games = _games.asStateFlow()

    init {
        viewModelScope.launch {
            val games = gameRepository.fetchGamesForAccountWithPlayers()
            _games.update { games }
            println(_games)
        }
    }


    fun loadData() {
        val games = gameRepository.fetchGamesForAccountWithPlayers()
        _games.update { games }
    }

    fun saveGame(name: String, slot: Number, players: List<Player>): Unit {
        val game = Game(slot = slot, name = name)
        gameRepository.save(game)
        players.forEach {
            playerRepository.save(it)
            gamePlayerRepository.save(game.id, it.playerId)
        }
        _games.update { it + game }
    }
}