package viewmodels

import dev.icerock.moko.mvvm.viewmodel.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import models.Character

class CharacterViewModel : ViewModel() {
    private val _character = MutableStateFlow<Character?>(null)
    val character = _character.asStateFlow()

    fun updateSelectedCharacter(character: Character) {
        _character.update { character }
    }
}