import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import be.kennymaes.database.AppDatabase
import moe.tlaster.precompose.PreComposeApp
import moe.tlaster.precompose.navigation.NavHost
import moe.tlaster.precompose.navigation.path
import moe.tlaster.precompose.navigation.rememberNavigator
import repository.GamePlayerRepositoryImpl
import repository.GameRepositoryImpl
import repository.PlayerRepositoryImpl
import ui.GameOptionsView
import ui.NewGameFormView
import ui.StartGameView
import ui.StatsView
import viewmodels.GameViewModel
import viewmodels.GamesViewModel

//

@Composable
fun App(driverFactory: DriverFactory) {
    PreComposeApp {

        val navController = rememberNavigator()
        val database: AppDatabase = createDatabase(driverFactory.createDriver())
        val gameViewModel by remember { mutableStateOf(GamesViewModel(GameRepositoryImpl(database), PlayerRepositoryImpl(database), GamePlayerRepositoryImpl(database))) }

        NavHost(navigator = navController, initialRoute = "startGame") {
            scene("startGame") {
                StartGameView(gameViewModel, navController)
            }
            scene("gameOptions/{id}") { backStackEntry ->
                val id: Long? = backStackEntry.path<Long>("id")
                GameOptionsView(navController, GameViewModel(id!!, GameRepositoryImpl(database), PlayerRepositoryImpl(database)))
            }
            scene("newGame/{slot}") { backStackEntry ->
                val slot: Int? = backStackEntry.path<Int>("slot")
                NewGameFormView(navController, slot, gameViewModel)
            }
            scene("currentGame/{id}") { backStackEntry ->
                val id: Long? = backStackEntry.path<Long>("id")
                StatsView(navController, GameViewModel(id!!, GameRepositoryImpl(database), PlayerRepositoryImpl(database)))
            }
        }
    }
}
