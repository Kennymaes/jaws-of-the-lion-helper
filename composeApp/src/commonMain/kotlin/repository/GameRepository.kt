package repository

import models.Game

interface GameRepository {

    fun fetchGamesForAccountWithPlayers(): List<Game>

    fun getById(id: Long): Game?
    fun save(game: Game): Long

    fun delete(id: Long): Unit
}