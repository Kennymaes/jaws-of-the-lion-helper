package repository

import be.kennymaes.database.AppDatabase
import models.Player
import models.Stats
import kotlin.random.Random.Default.nextLong

class PlayerRepositoryImpl(private val database: AppDatabase): PlayerRepository {
    override fun save(player: Player): Long {
        val playerQueries = database.`1_PlayersQueries`
        val statsQueries = database.`2_StatsQueries`
        playerQueries.insertPlayer(player.playerId, player.name, player.name)
        statsQueries.insertStats(
            nextLong(),
            player.playerId,
            player.stats.health.toLong(),
            player.stats.mana.toLong(),
            player.stats.coins.toLong()
        )
        return player.playerId
    }

    override fun delete(id: Long) {
        val playerQueries = database.`1_PlayersQueries`
        val statsQueries = database.`2_StatsQueries`
        val stats = statsQueries.getForPlayerId(id).executeAsOne()
        statsQueries.deleteStats(stats.stats_id)
        playerQueries.deletePlayer(id)
    }

    override fun saveStats(stats: Stats, playerId: Long) {
        val statsQueries = database.`2_StatsQueries`
        statsQueries.insertStats(stats.statsId, playerId, stats.health.toLong(), stats.mana.toLong(), stats.coins.toLong())
    }
}