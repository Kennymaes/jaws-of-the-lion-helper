package repository

import be.kennymaes.database.AppDatabase
import models.Player
import models.Stats
import kotlin.random.Random.Default.nextLong

class GamePlayerRepositoryImpl(private val database: AppDatabase): GamePlayerRepository {
    override fun save(gameId: Long, playerId: Long): Long {
        val gamePlayerQueries = database.`3_GamePlayersQueries`
        gamePlayerQueries.insertGamePlayer(gameId, playerId)
        return gameId;
    }

    override fun delete(gameId: Long, playerId: Long) {
        val gamePlayerQueries = database.`3_GamePlayersQueries`
            .deleteGamePlayer(gameId, playerId)
    }
}