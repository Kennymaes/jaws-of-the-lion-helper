package repository

import be.kennymaes.database.Games
import models.Game
import models.Player

interface GamePlayerRepository {
    fun save(gameId: Long, playerId: Long): Long

    fun delete(gameId: Long, playerId: Long): Unit
}