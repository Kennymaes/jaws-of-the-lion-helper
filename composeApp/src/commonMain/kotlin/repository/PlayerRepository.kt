package repository

import models.Player
import models.Stats

interface PlayerRepository {
    fun save(player: Player): Long

    fun delete(id: Long): Unit

    fun saveStats(stats: Stats, playerId: Long): Unit
}