package repository

import be.kennymaes.database.AppDatabase
import kotlinx.datetime.Clock.System.now
import models.Game
import models.Player
import models.Stats

class GameRepositoryImpl(private val database: AppDatabase) : GameRepository {

    override fun fetchGamesForAccountWithPlayers(): List<Game> {
        val gameQueries = database.`4_GamesQueries`
        val gamesForAccount = gameQueries.getAllGamesForAccount(123L).executeAsList()
        return gamesForAccount.map {
            val players = findPlayers(it.game_id);
            Game(it.game_id, it.slot, it.name, it.creationDate, players)
        }
        // Process or return the list of users with their orders
    }

    override fun getById(id: Long): Game? {
        val gameQueries = database.`4_GamesQueries`
        val gameDto = gameQueries.getById(id).executeAsOne()
        val players = findPlayers(gameDto.game_id)
        players.forEach {

        }
        return Game(gameDto.game_id, gameDto.slot, gameDto.name, gameDto.creationDate, players)
    }

    override fun save(game: Game): Long {
        val queries = database.`4_GamesQueries`
        queries.insertGame(game.id, 123L, game.name, game.slot.toLong(), now().toString())
        return game.id;
    }

    override fun delete(id: Long) {
        val queries = database.`4_GamesQueries`
        queries.deleteGame(id)
    }

    private fun findPlayers(gameId: Long): List<Player> {
        val playerQueries = database.`1_PlayersQueries`
        val gamePlayerQueries = database.`3_GamePlayersQueries`
        val statsQueries = database.`2_StatsQueries`
        val playerIds =
            gamePlayerQueries.getByGameId(gameId).executeAsList().map { test -> test.player_id }
        val players = mutableListOf<Player>()
        playerIds.forEach {
            val playerDto = playerQueries.getById(it!!).executeAsOne()
            val statsDto = statsQueries.getForPlayerId(it).executeAsOne();
            players.add(
                Player(
                    playerDto.player_id,
                    playerDto.name!!,
                    Stats(statsId = statsDto.stats_id, health = statsDto.health!!, mana = statsDto.mana!!, coins = statsDto.coins!!)
                )
            )
        }

        return players
    }
}
