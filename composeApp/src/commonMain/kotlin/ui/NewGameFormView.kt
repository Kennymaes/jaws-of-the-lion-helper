package ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Chip
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.FabPosition
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import models.Player
import models.Stats
import moe.tlaster.precompose.navigation.Navigator
import viewmodels.GamesViewModel

@Composable
fun NewGameFormView(navController: Navigator, slot: Number?, gamesViewModel: GamesViewModel) {
    var name by remember { mutableStateOf("") }
    var showDialog by remember { mutableStateOf(false) }
    var players: List<Player> by remember { mutableStateOf(emptyList()) }
    Scaffold(
        floatingActionButton = {
            AddPlayerFloatButton {
                showDialog = true
            }
        },
        floatingActionButtonPosition = FabPosition.End
    ) {
        Column(
            Modifier.padding(30.dp).fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text("New Game Slot $slot", fontSize = 30.sp)
                OutlinedTextField(
                    isError = name.length < 5,
                    singleLine = true,
                    value = name,
                    onValueChange = { name = it },
                    label = { Text("Game Name") },
                )
                Column(
                    verticalArrangement = Arrangement.spacedBy(20.dp),
                ) {
                    Text(
                        "Players",
                        textAlign = TextAlign.Start,
                        fontSize = 28.sp,
                        modifier = Modifier.fillMaxWidth().padding(top = 20.dp)
                    )
                    players.forEachIndexed { index, p ->

                        Card(
                            shape = RoundedCornerShape(12.dp),
                            backgroundColor = Color(128, 136, 148)
                        ) {
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                verticalAlignment = Alignment.CenterVertically,
                                modifier = Modifier.padding(
                                    top = 5.dp,
                                    bottom = 5.dp,
                                    start = 20.dp,
                                    end = 20.dp
                                ).fillMaxWidth()
                            ) {
                                Text("P$index| ${p.name}", fontSize = 25.sp)
                                Button(onClick = {
                                    players = players.filterNot { it.name == p.name }
                                }) {
                                    Icon(
                                        imageVector = Icons.Filled.Delete,  // Choose appropriate icon
                                        contentDescription = "Localized description"  // Space between icon and text
                                    )
                                }
                            }
                        }
                    }
                    if (players.isEmpty()) {
                        Text("No players created, Add at least one player")
                    }
                }
            }

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Button(enabled = isFormValid(name, players),
                    modifier = Modifier.width(120.dp), onClick = {
                        gamesViewModel.saveGame(name, slot ?: 1, players)
                        navController.navigate("startGame")
                    }) {
                    Text("Save")
                }
                Button(
                    onClick = { navController.popBackStack() },
                    modifier = Modifier.width(120.dp)
                ) {
                    Text("Cancel")
                }
            }

            // DIALOG Window
            if (showDialog) {
                Dialog(
                    onDismissRequest = { showDialog = false },
                    properties = DialogProperties(usePlatformDefaultWidth = false)
                ) {
                    Surface(modifier = Modifier.background(Color.White).fillMaxWidth()) {
                        SelectCharacterView(players) {
                            players = players + Player(
                                name = it.name,
                                stats = Stats(health = it.health, mana = 1, coins = 0)
                            )
                            showDialog = false
                        }
                    }
                }
            }
        }
    }
}

fun isFormValid(name: String, players: List<Player>): Boolean {
    return name.isBlank().not().and(name.length >= 5).and(players.isNotEmpty())
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun InputChipExample(
    text: String,
    onDismiss: () -> Unit,
) {
    var enabled by remember { mutableStateOf(true) }
    if (!enabled) return

    Chip(
        onClick = {
            onDismiss()
            enabled = !enabled
        }) {
        Text(text)
    }
}

@Composable
fun AddPlayerFloatButton(addPlayer: () -> Unit) {
    ExtendedFloatingActionButton(
        onClick = { addPlayer() },
        icon = { Icon(Icons.Filled.Add, "Extended floating action button.") },
        text = { Text(text = "Add player") },
        modifier = Modifier.padding(bottom = 80.dp)
    )
}