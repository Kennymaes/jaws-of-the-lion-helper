package ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import models.Character
import models.Player
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource

@Composable
fun SelectCharacterView(currentSelectedPlayers: List<Player>, onNavigate: (Character) -> Unit) {

    fun characters() = listOf(
        Character("Voidwarden", "voidwarden", 6, 11),
        Character("Demolitionist", "demolitionist", 8, 9),
        Character("Hatchet", "hatchet", 8, 10),
        Character("Red Guard", "red_guard", 10, 10),
    )

    MaterialTheme {
        var selectedCharacter: Character? by remember { mutableStateOf(null) }

        Column(
            Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                SelectCharacter(currentSelectedPlayers, characters(), selectCharacter = { selectedCharacter = it})
                CharacterInformation(selectedCharacter)
            }
            Button(
                onClick = {
                    onNavigate(
                        selectedCharacter
                            ?: throw IllegalArgumentException("No character selected")
                    )
                },
                enabled = selectedCharacter != null,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            ) {
                Text("Add")
            }
        }
    }
}

@Composable
private fun SelectCharacter(currentSelectedPlayers: List<Player>, characters: List<Character>, selectCharacter: (Character) -> Unit) {
    var showCharacters by remember { mutableStateOf(false) }
    Box {
        Button(
            onClick = { showCharacters = true },
            Modifier.padding(50.dp)
        ) {
            Text(text = "Select character")
        }
        DropdownMenu(
            expanded = showCharacters,
            onDismissRequest = { showCharacters = false },
            modifier = Modifier.fillMaxWidth()
        ) {
            characters.filter { character -> currentSelectedPlayers.map { it.name }.contains(character.name).not() }.forEach {
                DropdownMenuItem(onClick = {
                    selectCharacter(it)
                    showCharacters = false
                }) {
                    Text(
                        it.name,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalResourceApi::class)
@Composable
fun CharacterInformation(selectedCharacter: Character?) {
    selectedCharacter?.let {
        Text(it.name, Modifier.padding(20.dp), fontSize = 35.sp)
        Image(
            painter = painterResource("characters/${it.imageName}.png"),
            contentDescription = it.imageName,
            modifier = Modifier.fillMaxWidth()
        )
        StatRow("health", it.health)
        StatRow("cards_in_hand", it.maxCards)
    }
}

@OptIn(ExperimentalResourceApi::class)
@Composable
fun StatRow(imagePath: String, value: Number) {
    Row(
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(top = 10.dp)
    ) {
        Image(
            painter = painterResource("icons/${imagePath}.png"),
            contentDescription = "health",
            Modifier.size(30.dp)
        )
        Spacer(Modifier.width(10.dp))
        Text("${value}", fontSize = 18.sp)
    }
}
