package ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import models.Game
import moe.tlaster.precompose.navigation.Navigator
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource
import viewmodels.GameViewModel

@Composable
fun StatsView(navController: Navigator, gameViewModel: GameViewModel) {
    var tabIndex by remember { mutableStateOf(0) }
    val game by gameViewModel.game.collectAsState()

    Column(
        Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        if (game == null) {
            CircularProgressIndicator()
        } else {
            println(game)
            LazyColumn(verticalArrangement = Arrangement.spacedBy(20.dp)) {
                item {
                    TabRow(selectedTabIndex = tabIndex) {
                        game!!.players.forEachIndexed { index, player ->
                            Tab(
                                selected = tabIndex == index,
                                onClick = { tabIndex = index },
                                text = { Text(player.name) }
                            )
                        }
                    }
                    // Content of the selected tab
                    tabContent(tabIndex, game!!, gameViewModel)
                }
            }
        }

        Button(
            onClick = { navController.popBackStack() },
            colors = ButtonDefaults.buttonColors(Color.Red),
            modifier = Modifier.fillMaxWidth().height(60.dp)
        ) {
            Text("Stop Playing", color = Color.White, fontSize = 25.sp)
        }
    }
}


@Composable
fun tabContent(index: Int, game: Game, gameViewModel: GameViewModel) {
    StatsImage("health")
    val player = game.players.get(index)
    StatsContent(
        player.stats.health.toString(),
        { gameViewModel.saveStats(player.stats.reduceHealth(), player.playerId) },
        { gameViewModel.saveStats(player.stats.addHealth(), player.playerId) }
    )
    StatsImage("mana")
    StatsContent(
        player.stats.mana.toString(),
        { gameViewModel.saveStats(player.stats.reduceMana(), player.playerId) },
        { gameViewModel.saveStats(player.stats.addMana(), player.playerId) }
    )
    StatsImage("coin")
    StatsContent(
        player.stats.coins.toString(),
        { gameViewModel.saveStats(player.stats.reduceCoins(), player.playerId) },
        { gameViewModel.saveStats(player.stats.addCoins(), player.playerId) }
    )
}

@Composable
fun StatsContent(value: String, reduce: () -> Unit, add: () -> Unit) {
    Row(
        horizontalArrangement = Arrangement.SpaceAround,
        modifier = Modifier.fillMaxWidth().padding(top = 10.dp)
    ) {
        roundButton(Icons.Default.ArrowBack) { reduce() }
        Text(text = value, fontSize = 30.sp)
        roundButton(Icons.Default.ArrowForward) { add() }
    }

}

@Composable
fun roundButton(icon: ImageVector, onClick: () -> Unit) {
    OutlinedButton(
        onClick = { onClick() },
        modifier = Modifier.size(35.dp),
        shape = CircleShape,
        border = BorderStroke(2.dp, Color(0XFF0F9D58)),
        contentPadding = PaddingValues(0.dp),
        colors = ButtonDefaults.outlinedButtonColors(contentColor = Color.Blue)
    ) {
        // Adding an Icon "Add" inside the Button
        Icon(icon, contentDescription = "content description", tint = Color(0XFF0F9D58))
    }
}

@OptIn(ExperimentalResourceApi::class)
@Composable
fun StatsImage(imageName: String) {
    Image(
        painter = painterResource("icons/${imageName}.png"),
        contentDescription = imageName,
        modifier = Modifier.fillMaxWidth().height(70.dp).padding(top = 30.dp)
    )
}