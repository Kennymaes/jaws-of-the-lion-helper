package ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import models.Game
import moe.tlaster.precompose.navigation.Navigator
import viewmodels.GameViewModel

@Composable
fun GameOptionsView(navController: Navigator, gameViewModel: GameViewModel) {
    val game by gameViewModel.game.collectAsState()
    Column(
        modifier = Modifier.fillMaxSize().padding(start = 20.dp, end = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(game?.name ?: "Game Options", fontSize = 35.sp, modifier = Modifier.padding(20.dp))
        OptionsMenu(gameViewModel, game, navController)
        Button(onClick = { navController.popBackStack() },
            modifier = Modifier.fillMaxWidth().height(70.dp).padding(bottom = 20.dp)) {
            Text("Back")
        }
    }
}

@Composable
fun OptionsMenu(gameViewModel: GameViewModel, game: Game?, navController: Navigator) {
    var showDialog by remember { mutableStateOf(false) }
    if (game == null) {
        CircularProgressIndicator(
            modifier = Modifier.width(64.dp),
        )
    } else {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(20.dp)
        ) {
            Button(
                onClick = {
                    navController.navigate("currentGame/${game.id}")
                },
                modifier = Modifier.fillMaxWidth().height(50.dp)
            ) {
                Text("Start/Resume Game")
            }
            Button(
                onClick = {},
                modifier = Modifier.fillMaxWidth().height(50.dp)
            ) {
                Text("Edit Game")
            }
            Button(
                onClick = {
                    showDialog = true
                },
                modifier = Modifier.fillMaxWidth().height(50.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Red)
            ) {
                Text("Delete Game", color = Color.White)
            }
        }
    }

    ConfirmDialog(
        showDialog,
        game?.name,
        {
            showDialog = false
        },
        {
            gameViewModel.delete(game?.id ?: throw IllegalArgumentException("No id to delete"))
                .also { navController.navigate("startGame") }
        }
    )
}

@Composable
fun ConfirmDialog(
    openDialog: Boolean,
    gameTitle: String?,
    dismiss: () -> Unit,
    confirm: () -> Unit
) {
    if (openDialog) {
        AlertDialog(
            onDismissRequest = {
                dismiss()
            },
            title = {
                Text(text = gameTitle ?: "Delete")
            },
            text = {
                Text(text = "Are you sure you want to delete this Game")
            },
            confirmButton = {
                TextButton(
                    colors = ButtonDefaults.buttonColors(Color.Green),
                    onClick = {
                        dismiss()
                        confirm()
                    }
                ) {
                    Text("Confirm")
                }
            },
            dismissButton = {
                TextButton(
                    colors = ButtonDefaults.buttonColors(Color.Red),
                    onClick = {
                        dismiss()
                    }
                ) {
                    Text("Dismiss", color = Color.White)
                }
            }
        )
    }
}