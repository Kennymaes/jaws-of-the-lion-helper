package ui

import Tokenizer
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.ExtendedFloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import models.Game
import moe.tlaster.precompose.navigation.Navigator
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource
import viewmodels.GamesViewModel

@Composable
fun StartGameView(gamesViewModel: GamesViewModel, navController: Navigator) {
    val games: List<Game> by gamesViewModel.games.collectAsState()
    var showDialog by remember { mutableStateOf(false) }
    Scaffold(floatingActionButton = {
        GetInfoFloatingActionButton {
            showDialog = true
        }
    }) {
        Column(
            Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                "Select Game",
                fontSize = 35.sp,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
            Gameslots(games, gamesViewModel, navController)
        }

        // DIALOG Window
        if (showDialog) {
            QuestionDialog { showDialog = false }
        }
    }
}

@Composable
fun Gameslots(games: List<Game>, gamesViewModel: GamesViewModel, navController: Navigator) {
    LazyVerticalGrid(
        columns = GridCells.Adaptive(160.dp),
        contentPadding = PaddingValues(8.dp)
    ) {
        items(4) { index ->
            GameSlotCard(games.find { it.slot == (index + 1L) }, index + 1, navController)
        }
    }

    LaunchedEffect(Unit) {
        gamesViewModel.loadData()
    }
}

@Composable
fun GameSlotCard(game: Game?, slot: Number, navController: Navigator) {
    Card(
        shape = RoundedCornerShape(12.dp),
        modifier = Modifier
            .padding(5.dp)
            .height(150.dp)
            .clickable {
                game?.let { navController.navigate("gameOptions/${game.id}") }
                    ?: navController.navigate("newGame/$slot")
            },
        backgroundColor = Color(55, 63, 81),
        elevation = 5.dp
    ) {
        game?.let {
            Column(Modifier.padding(20.dp)) {
                Text(
                    game.name,
                    fontSize = 20.sp,
                    color = Color.White,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                GameInformationRow("calendar", game.creationDate.toString())
                GameInformationRow("pawn", game.playerSize().toString())
            }

        } ?: Text(
            "Empty slot",
            fontSize = 20.sp,
            color = Color.White,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxSize().padding(top = 30.dp)
        )
    }
}

@OptIn(ExperimentalResourceApi::class)
@Composable
fun GameInformationRow(imagePath: String, value: String) {
    Row(
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(top = 10.dp)
    ) {
        Image(
            painter = painterResource("icons/${imagePath}.png"),
            contentDescription = "health",
            Modifier.size(20.dp)
        )
        Spacer(Modifier.width(10.dp))
        Text(value, color = Color.White)
    }
}

@Composable
fun GetInfoFloatingActionButton(addPlayer: () -> Unit) {
    ExtendedFloatingActionButton(
        onClick = { addPlayer() },
        icon = { Icon(Icons.Filled.Info, "Extended floating action button.") },
        text = { Text("More info") },
        modifier = Modifier.padding(bottom = 20.dp)
    )
}

@Composable
fun QuestionDialog(dismissDialog: () -> Unit) {
    var question by remember { mutableStateOf("") }
    val answer by remember { mutableStateOf("") }
    Dialog(
        onDismissRequest = { dismissDialog() },
        properties = DialogProperties(usePlatformDefaultWidth = false)
    ) {
        Surface(modifier = Modifier.background(Color.White).fillMaxWidth().fillMaxHeight()) {
            Column(verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally) {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Text("QuestionHelper", fontSize = 35.sp)
                    OutlinedTextField(
                        singleLine = true,
                        value = question,
                        onValueChange = { question = it },
                        label = { Text("Question") },
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done)
                    )
                    Text(answer)
                }

                Column {
                    Button(
                        onClick = { resolveQuestion(question) },
                        enabled = question.isNotEmpty(),
                        modifier = Modifier.padding(bottom = 20.dp)
                    ) {
                        Text("Search")
                    }

                    Button(
                        onClick = { dismissDialog() },
                        modifier = Modifier.padding(bottom = 20.dp)
                    ) {
                        Text("Close")
                    }
                }
            }
        }
    }
}

fun resolveQuestion(question: String): String {
    println(Tokenizer().tokenize(question))
    return ""
}